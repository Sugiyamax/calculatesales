package jp.alhinc.keita_sugiyama.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) throws IOException {
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		BufferedReader br = null;

		FileReader fr = null;
		HashMap<String, String> hmCodeName = new HashMap<String,String>();
		HashMap<String, Long> hmCodeSales = new HashMap<String, Long>();
		inputFile(args[0],"branch.lst","支店定義ファイル", hmCodeName, hmCodeSales, br, fr);

		try{
			File file = new File(args[0],"branch.lst");
			if(file.exists()){
				fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				while((line = br.readLine()) != null) {
					String[] strs = line.split(",");
					if(strs[0].matches("[0-9]{3}") == true && strs.length == 2){
						hmCodeName.put(strs[0] ,strs [1]);
						hmCodeSales.put(strs[0] ,0L);
					}else{
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
				}
			}else {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
		} catch(IOException e){
			System.out.println("エラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try{
					fr.close();
					br.close();
				} catch(IOException e) {
					System.out.println(e);
					return;
				}
			}

		}



		try{
			File dir = new File(args[0]);
			String[] list2 = dir.list();
			List<String> rcdFileList = new ArrayList<String>();
			for(int i = 0; i < list2.length ; i++){
				File f = new File(args[0], list2[i]);
				if(list2[i].matches("[0-9]{8}.rcd") == true && f.isFile() ){
					rcdFileList.add(list2[i]);
				}
			}
			for(int i = 0; i < rcdFileList.size() -1; i++){
				String[] splitNameRcd = rcdFileList.get(i).split("\\.");
				int intFileNum =Integer.parseInt(splitNameRcd[0]);
				String[] nextFileNum = rcdFileList.get(i + 1).split("\\.");
				int intNextNum = Integer.parseInt(nextFileNum[0]);
				if(intNextNum - intFileNum  == 1 ){
				}else{
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			for (int i = 0; i < rcdFileList.size(); i++){
				File file2 = new File(args[0], rcdFileList.get(i));
				List<String> inFile = new ArrayList<String>();
				String line2;
				fr = new FileReader(file2);
				br = new BufferedReader(fr);
				while((line2 = br.readLine()) != null) {
					inFile.add(line2);
				}
				if(inFile.size() != 2){
					System.out.println(rcdFileList.get(i) + "のフォーマットが不正です");
					return;
				}
				if(hmCodeSales.containsKey(inFile.get(0)) != true){
					System.out.println(rcdFileList.get(i)+"の支店コードが不正です");
					return;
				}
				if(inFile.get(1).matches("[0-9]{1,}")){
					Long sales = Long.parseLong(inFile.get(1)) + hmCodeSales.get(inFile.get(0));
					if(sales < 10000000000L){
						hmCodeSales.put(inFile.get(0) ,sales);
					}else{
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}else{
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}

		}catch(IOException e){
			System.out.println("エラーが発生しました");
			return;
		} finally{
			if(br != null) {
				try{
					fr.close();
					br.close();
				} catch(IOException e) {
					System.out.println(e);
					return;
				}
			}

		}

		outputFile (args[0], "branch.out", hmCodeName, hmCodeSales);
	}


	public static boolean inputFile(String dirPath, String fileName, String comment,
			HashMap<String, String> codes,
			HashMap<String, Long> names,
			BufferedReader br, FileReader fr) {

		try{
			File file = new File(dirPath,fileName);
			if(file.exists()){
				fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				while((line = br.readLine()) != null) {
					String[] strs = line.split(",");
					if(strs[0].matches("[0-9]{3}") == true && strs.length == 2){
						codes.put(strs[0] ,strs [1]);
						names.put(strs[0] ,0L);
					}else{
						System.out.println(comment+"のフォーマットが不正です");
						return false;
					}
				}
			}else {
				System.out.println(comment +"ファイルが存在しません");
				return false;
			}
		}catch(IOException e){
			System.out.println("エラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try{
					fr.close();
					br.close();
				} catch(IOException e) {
					System.out.println(e);
					return false;
				}
			}

		}
		return false;

	}


	public static boolean outputFile(String dirPath, String fileName,
			Map<String,String> codes, Map<String , Long> names)
	{
		PrintWriter pw = null;
		BufferedWriter bw = null;
		try{
			File fileOp = new File (dirPath,fileName);

			FileWriter fw = new FileWriter(fileOp);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);
			for(Map.Entry<String, String> kv : codes.entrySet()){
				bw.write(kv.getKey() + "," + kv.getValue() + "," + names.get(kv.getKey()));
				bw.newLine();
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally{
				if(pw != null) {
					try {
						pw.close();
						bw.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
		}
		return true;
	}
}


